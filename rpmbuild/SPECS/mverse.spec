Name:           mverse
Version:        1
Release:        0
Summary:        simple cli to create multiple development environments in linux 
BuildArch:      noarch

License:        GPL
#URL:            
Source0:        https://gitlab.com/jrtlabs/mverse/-/raw/master/files/mverse-1.tar.gz 

#BuildRequires:  
Requires:       bash docker-ce

%description
This package allows users to create impromptu development environments using docker and linux cli

%prep

%build

%install
mkdir -p %{buildroot}/%{_bindir}
mkdir -p %{buildroot}/%{_sysconfdir}/jrtlabs

tar -xvf %{SOURCE0} -C %{buildroot}/%{_sysconfdir}/jrtlabs
ln -s --relative %{buildroot}/%{_sysconfdir}/jrtlabs/mverse %{buildroot}/%{_bindir}/mverse 
chmod -R 775 %{buildroot}/%{_sysconfdir}/jrtlabs
chmod -R 775 %{buildroot}/%{_bindir}/mverse

%clean
rm -rf ${buildroot}

%files
%attr(0775, root, root)
%{_bindir}/mverse
%{_sysconfdir}/jrtlabs
%{_sysconfdir}/jrtlabs/cpp
%{_sysconfdir}/jrtlabs/node
%{_sysconfdir}/jrtlabs/go
%{_sysconfdir}/jrtlabs/dpdk

%{_sysconfdir}/jrtlabs/mverse

%{_sysconfdir}/jrtlabs/cpp/Dockerfile
%{_sysconfdir}/jrtlabs/cpp/Dockerfile.extras
%{_sysconfdir}/jrtlabs/cpp/run

%{_sysconfdir}/jrtlabs/dpdk/Dockerfile
%{_sysconfdir}/jrtlabs/dpdk/Dockerfile.extras
%{_sysconfdir}/jrtlabs/dpdk/run

%{_sysconfdir}/jrtlabs/go/Dockerfile
%{_sysconfdir}/jrtlabs/go/Dockerfile.extras
%{_sysconfdir}/jrtlabs/go/run

%{_sysconfdir}/jrtlabs/node/Dockerfile
%{_sysconfdir}/jrtlabs/node/Dockerfile.extras
%{_sysconfdir}/jrtlabs/node/run

%changelog
* Thu Aug  26 2021 Taylor Kaplan <taylor@jrtlabs.com> - 0.1
- First version being packaged
