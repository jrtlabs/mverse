#!/bin/bash
export PRJ_DIR=$(pwd)
rm -rf rpmbuild/SOURCES
mkdir -p rpmbuild/SOURCES
cd $PRJ_DIR/rpmsources && tar -czvf mverse-1.tar.gz .
cd $PRJ_DIR && mv rpmsources/mverse-1.tar.gz rpmbuild/SOURCES
rpmbuild --define "_topdir $PRJ_DIR/rpmbuild" -bb rpmbuild/SPECS/mverse.spec
