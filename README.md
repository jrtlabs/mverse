# MVerse

Mverse is a convenient script used to mount your current work directory into a particular docker environment for development. We support go, node, cpp, and dpdk. Lets take for example you have a c++ project and you want to use the latest c++ available. Simply navigate to your c++ project and run.

```
$ cd my-cpp-project
$ ls
build  CMakeLists.txt  configure  Dockerfile  include  install_extras.sh  run  src  toolchains  vendor
$ g++
bash: g++: command not found...
$ mverse cpp
Initializalizing environment
[root@c56fda8558a4 src]# ls
build  CMakeLists.txt  configure  Dockerfile  include  install_extras.sh  run  src  toolchains  vendor
[root@c56fda8558a4 src]# g++
Copyright (C) 2020 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

[root@c56fda8558a4 src]# g++
```

We only support limited environments however we plan on adding options to support more languages and more environments for each language. You can also add a custom install script that will run for your projects environment when you mount into your environment.

## Custom install script

To use a custom install script to install your favorite libraries, just a file called install_extras.sh

```
#!/bin/bash
# Example of your_project/install_extras.sh
#!/bin/bash

# Install Robin Hood Map
git clone https://github.com/Tessil/robin-map.git /tmp/robin-map
mv /tmp/robin-map/include/* /usr/local/include
rm -rf /tmp/robin-map
# End Robin Hood Map

# Install nlohmann
git clone https://github.com/nlohmann/json.git /tmp/nlohmann
mv /tmp/nlohmann/include/nlohmann /usr/local/include/
rm -rf /tmp/nlohmann/
# End install nlohmann

# Install boost/sml
git clone https://github.com/boost-ext/sml.git /tmp/sml
cp -rf /tmp/sml/include/boost/* /usr/include/boost
rm -rf /tmp/sml
# End install sml

```

When you then run the normal mverse cpp script as such

```
$ mverse cpp
Initializalizing environment
[root@c56fda8558a4 src]# ls
build  CMakeLists.txt  configure  Dockerfile  include  install_extras.sh  run  src  toolchains  vendor
[root@c56fda8558a4 src]# g++
Copyright (C) 2020 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

[root@c56fda8558a4 src]# ls /usr/include/boost
... output showing your installed extra
```

mverse picks up your install_extras.sh. You will however need to delete the docker image if it was already created since we do cache docker images.

## Installing mverse

**Requires Docker To Work

Before install mverse, we work with docker to provide the environments you want. Please install docker first then proceed.

We wanted mverse to be a debian and rpm package from the get go. So you we provided some scripts to help build the packages and install them. Currently we only support rpms. to build and install do the following:

```
$ ./make
$ sudo ./install
```

Now you should have mverse! Enjoy. 
